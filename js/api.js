async function fetchAsync (url) {
    let response = await fetch(url);
    let data = await response.json();
    return data;
}
function create(ele, attrs) {
    var element = document.createElement(ele);
    for (let val in attrs) {
        element.setAttribute(val, attrs[val]);
    }
    return element;
}
function getBgColor(){
    items = ["#4d7e65", "#b5533c", "#dddddd"]
    return items[Math.floor(Math.random()*items.length)];
}
function createSection(item, today=false){
    section = create('section', {'data-background': getBgColor(), 'data-transition': "slide", 'data-background-transition':"zoom", 'class':'termine'})
    h1 = create('h1', {'class':''})
    h1.innerHTML = item.title
    
    h4 = create('h4')
    if (today == false){
        h4.innerHTML = item.weekday + " " + item.day + ". " + item.month
    }else {
        h4.innerHTML = "HEUTE"
    }
    
    small = create('small', {'class': 'd-block'})
    small.innerHTML = "mit: " + item.speaker

    row = create('div', {'class': 'row justify-content-center'})
    col6 = create('div', {'class': 'col-12 col-md-4'})
    if (item.image != undefined && item.image != ""){
        path = "https://fcg-schoemberg.de/storage/" + item.image.replaceAll("\\", "/")
        img = create('div', {'width': '75%', 'style':'border-radius: 19%;height: 220px;width: 220px;background:url(' + path + ') no-repeat center;background-size:cover;margin-left: auto;margin-right: auto;'})
        col6.append(img)
    }
    col6.append(small)
    row.append(col6)

    col6_2 = create('div', {'class': 'col-12 col-md-4 align-items-center justify-content-center d-flex'})
    
    divWrapper = create('div')

    h3 = null
    if (today == false){
        h3 = create('h3')
        h3.innerHTML = item.time + " Uhr"
    }else {				
        h3 = create('h3', {'id': 'timer'})
        start = new Date(item.date + " " + item.time);
    }
    h5 = create('h5')
    h5.innerHTML = "Ort: " + item.location
    
    divWrapper.append(h3)
    divWrapper.append(h5)
    if (item.information != null && item.information != ""){
        span = create('span', {'class':'px-3', 'style':'font-size: 14pt;font-family: revert;display: block;color: #ffffffa1!important;'})
        span.innerHTML = "Information: " + item.information
        divWrapper.append(span)
    }
    col6_2.append(divWrapper)				
    row.append(col6_2)

    section.append(h1)
    section.append(h4)
    section.append(row)
    slides.append(section)
}
function createSectionHighlight(item){
    section = create('section', {'data-background': getBgColor(), 'data-transition': "slide", 'data-background-transition':"zoom", 'class':'special'})
    h1 = create('h1', {'class':'text-start m-0', 'style':'padding-left: 20px;'})
    h1.innerHTML = item.title
    
    h4 = create('h3', {'class':'text-start m-0', 'style':'padding-left: 40px;'})
    h4.innerHTML = item.weekday + " " + item.day + ". " + item.month + " - " + item.time + " Uhr"
    h5 = create('h5', {'class':'text-start m-0', 'style':'padding-left: 40px;'})
    h5.innerHTML = "Ort: " + item.location


    small = create('small', {'class': 'd-block'})
    small.innerHTML = "mit: " + item.speaker

    row = create('div', {'class': 'row'})
    col6 = create('div', {'class': 'col-12 col-md-6 pt-5 mt-5'})
    path = "img/default.png"
    if (item.image != undefined && item.image != ""){
        path = "https://fcg-schoemberg.de/storage/" + item.image.replaceAll("\\", "/")
    }
    img = create('div', {'width': '75%', 'style':'border-radius: 19%;height: 150px;width: 150px;background:url(' + path + ') no-repeat center;background-size:cover;margin-left: auto;margin-right: auto;'})
    col6.append(img)

    col6.append(small)

    if (item.information != null && item.information != ""){
        span = create('span', {'class':'px-3 mt-5', 'style':'font-size: 14pt;font-family: revert;display: block;color: #ffffffa1!important;'})
        span.innerHTML = "Information: " + item.information
        col6.append(span)
    }

    row.append(col6)

    col6_2 = create('div', {'class': 'col-12 col-md-4 align-items-center justify-content-center d-flex'})
    

    col6_3 = create('div', {'class': 'col-12 col-md-4'})
    if (item.file != undefined && item.file != ""){
        path = "https://fcg-schoemberg.de/storage/" + item.file.replaceAll("\\", "/")
        doc = create('div', {'style':'height: 1000px;width: 500px;background:url(' + path + ') no-repeat top; background-size: contain; position: absolute; margin-left: 20px; margin-top: -120px; box-shadow: 1px 1px 17px #586e75;'})
        col6_3.append(doc)
    }
    row.append(col6_3)

    section.append(h1)
    section.append(h4)
    section.append(h5)
    section.append(row)
    slides.append(section)
}

function createImageSection(item) {
    section = create('section', {'data-background': getBgColor(), 'data-transition': "slide", 'data-background-transition':"zoom", "style": "width:100%;height:100%;"})
    row = create('div', {'class': 'row justify-content-center align-items-center', "style": "width:100%;height:100%;"})
    
    path = "https://fcg-schoemberg.de/storage/" + item.slider_image.replaceAll("\\", "/")
    img = create('img', {'src':path, 'width': '100%', 'height': '100%', "style": "width:auto;height:auto"})
    row.append(img)

    section.append(row)
    slides.append(section)
}

async function selectEvents(){
    data = await fetchAsync("https://fcg-schoemberg.de/api/events")
    data.forEach(function(item){
        if (item.highlight == "1"){
            createSectionHighlight(item)
        }
        else if (new Date(item.date)<new Date(Date.now())){
            createSection(item, today=true)
            tick()

        }
        else if(new Date(item.date)<new Date(Date.now() + 12096e5)){
            createSection(item)
        }
    })
    initPresentation()
    contents = getContents()
}
events = selectEvents()


async function getContents() {
    data = await fetchAsync("https://fcg-schoemberg.de/api/contents")
    data.forEach(function(item){
        createImageSection(item)
    })
}

// TIMER START
var start = null
function pad(num) {
    return ("0" + parseInt(num)).substr(-2);
}

function tick() {
    var now = new Date();
    var remain = (start - now) / 1000;
    var hh = pad((remain / 60 / 60) % 60);
    var mm = pad((remain / 60) % 60);
    var ss = pad(remain % 60);
    if (!(isNaN(hh)||isNaN(mm)||isNaN(ss)||remain < 0)){
        document.getElementById('timer').innerHTML = hh + ":" + mm + ":" + ss;
    }else {
        document.getElementById('timer').innerHTML = "00:00";
    }
    if (! (now > start)) { // too late, go to tomorrow
        setTimeout(tick, 1000);
    }
}
// TIMER END