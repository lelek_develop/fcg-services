"use strict"
// AUDIO START
let files = [
    "easy-lifestyle-137766.mp3",
    "inspiring-cinematic-background-music-for-videos-5717.mp3",
    "inspiring-epic-cinematic-mood-13556.mp3",
    "inspiring-motivational-mood-14107.mp3",
    "morning-garden-acoustic-chill-15013.mp3",
    "motivated-14108.mp3",
    "the-beat-of-nature-122841.mp3"
]
let getNextAudio = function(){
    return "./audio/" + files[Math.floor(Math.random()*files.length)];
}
let audio = function(){
    var audioPlayer = document.getElementById('audioPlayer');
    audioPlayer.src = getNextAudio();
    audioPlayer.onended = function(){
        audioPlayer.src = getNextAudio();
    }
    try {
        audioPlayer.play();
        return true
    }
    catch(e) {
        return false
    }

}

if (findGetParameter("audio") === 'true'){
    audio()
}


function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
// AUDIO END
